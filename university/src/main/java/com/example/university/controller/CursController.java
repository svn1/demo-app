package com.example.university.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/curs")
public class CursController {

    @GetMapping()
    public String getCourses(){
        return "Courses";
    }

}
